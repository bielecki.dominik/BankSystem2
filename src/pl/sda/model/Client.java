package pl.sda.model;

import java.util.ArrayList;
import java.util.List;

public class Client extends User {
	
	private String address;
	private List<AbstractAccount> accounts;   // po to zebysmy mogli wrzucac inne rodzaje kont do jednego klienta

	public Client(String name, String surname, String password, String address) {
		super(name, surname, password);
		this.address = address;
		accounts = new ArrayList<>();
		accounts.add(new CurrentAccount()); // w konstrukturze od razu tworzymy konto do kazdego nowego klienta
	}
	
	public Client(String name, String surname, String address, String login, String password){
		this(name, surname, password, address);
		this.login = login ;    // konstruktor stworzony na potrzeby zapisywanie i odtwarzania z pliku
		this.accounts.clear();
	}

	public String getAdress() {
		return address;
	}

	public void setAdress(String adress) {
		this.address = adress;
	}

	public List<AbstractAccount> getAccounts() {
		return accounts;
	}

	@Override
	public String toString() {
		return "Client [address=" + address + ", accounts=" + accounts + ", name=" + name + ", surname=" + surname
				+ ", login=" + login + ", password=" + password + "]";
	}
	


}
