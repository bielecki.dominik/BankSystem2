package pl.sda.model;

public abstract class User {
	
	protected String name;
	protected String surname;
	protected String login;
	protected String password;		// w wolnym czasie zrobic cyfrem Cezara
	
	public User(String name, String surname, String password) {
		super();
		this.name = name;
		this.surname = surname;
		this.login = name + "." + surname; //login bedzie utwarzany automatycznie
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	

}
